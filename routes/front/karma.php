<?php

use App\Http\Controllers\Front\KarmaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'v1'], function ($router) {
    Route::get('user/{id}/{limit}/{action}', [KarmaController::class, 'getKarmaActions']);
});
