<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->unique()->name(),
            'karma_score' => $this->faker->numberBetween(0,20000),
            'image_id' => Image::factory()->create(),
        ];
    }
}
