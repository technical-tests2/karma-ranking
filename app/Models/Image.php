<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Image extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['url'];


    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }


}
