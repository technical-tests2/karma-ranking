<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    use ModelTrait, HasFactory;

    public $timestamps = false;
    protected $fillable = ['username', 'karma_score', 'image_id'];
    protected $minimumAllowedKey = ['username', 'karma_score'];
    protected $notAllowedKey = [];
    protected $specialFields;
    protected $extraFields;

    public function __construct(array $attributes = array())
    {
        $this->specialFields = [];
        $this->extraFields = [
            'image_url' => function () {
                $image = $this->image;
                if ($image) {
                    return $image->url;
                }
                return null;
            },
            'position' => function(){
                $users = collect(DB::select('SELECT id,karma_score, @curRank := @curRank + 1 AS rank FROM users p , (SELECT @curRank := 0) r WHERE karma_score >= ' . $this->karma_score . ' ORDER BY karma_score DESC, id ASC;'));
                $data  = $users->where('id', $this->id);
                $target = $data->values()->first();
                if($target){
                    return $data->values()->first()->rank;
                }
                return null;
            }
        ];
        parent::__construct($attributes);
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id', 'id');
    }

    protected static function newFactory(): UserFactory
    {
        return UserFactory::new();
    }



}
