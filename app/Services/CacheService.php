<?php

namespace App\Services;


use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CacheService
{


    /**
     * @param $tag
     * @param $url
     * @param $options
     * @return array|mixed|null
     */
    public function getItemFromCache($tag, $url, $options)
    {
        try {
            $cacheKey = $url . implode(Arr::flatten($options));
            $item = Cache::tags([$tag])->get($cacheKey);
            if ($item) {
                return json_decode($item,true);
            }
            return $item;
        } catch (\Exception $e) {
            Log::debug('cache Exception: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * @param $tag
     * @param $url
     * @param $options
     * @param $cacheItem
     */
    public function saveItemInCache($tag, $url, $options, $cacheItem)
    {
        try {
            $cacheKey = $url . implode(Arr::flatten($options));
            $expiresAt = Carbon::now()->addHours(24);
            Cache::tags([$tag])->put($cacheKey, json_encode($cacheItem),$expiresAt);
        } catch (\Exception $e) {
            Log::debug('cache Exception: ' . $e->getMessage());
        }
    }

    public function removeTagCache($tag){
        try {
            Cache::tags([$tag])->flush();
        } catch (\Exception $e) {
            Log::debug('cache Exception: ' . $e->getMessage());
        }
    }


}
