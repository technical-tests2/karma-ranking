<?php

namespace App\Services\Front;


use App\Models\User;
use Illuminate\Support\Facades\DB;

class KarmaService
{
    public function getKarma($userId, $limit): array
    {
        try {
            $user = User::where('id', $userId)->get()->first();
            if ($user) {
                if ($limit > 100) {
                    $numberOfReturnedUsers = 100;
                } else {
                    $numberOfReturnedUsers = $limit;
                }

                $numberOfSemiUsersWithMain = floor($numberOfReturnedUsers / 2) + 1;

                $lowerUsers = User::where('karma_score', '<', $user->karma_score)
                    ->orWhere(function ($qry) use ($user) {
                        $qry->where('karma_score', '=', $user->karma_score)
                            ->where('id', '>=', $user->id);
                    })->orderBy('karma_score', 'desc')->orderBy('id')->take($numberOfReturnedUsers)->get()->all();
                $numberOfHigherUsers = $numberOfSemiUsersWithMain;
                if (count($lowerUsers) < $numberOfSemiUsersWithMain) {
                    $numberOfHigherUsers = $numberOfReturnedUsers - count($lowerUsers) + 1;
                }
                $higherUsers = User::where('karma_score', '>', $user->karma_score)
                    ->orWhere(function ($qry) use ($user) {
                        $qry->where('karma_score', '=', $user->karma_score)
                            ->where('id', '<=', $user->id);
                    })->orderBy('karma_score', 'asc')->orderBy('id', 'desc')->take($numberOfHigherUsers)->get()->all();
                $mainUser = $lowerUsers[0];
                array_shift($lowerUsers);
                array_shift($higherUsers);
                $users = $this->getRankUsersFormat($mainUser, $lowerUsers, array_reverse($higherUsers), $numberOfReturnedUsers);
                $modal = new User();
                return ['code' => 200, 'data' => ['users' => $modal->transformList($users, ['id', 'position', 'karma_score', 'image_url'])]];
            }
            return ['code' => 404, 'data' => ['message' => 'user not found']];
        } catch (\Exception $e) {
            throw  $e;
        }

    }

    public function getRankUsersFormat($mainUser, $lowerUsers, $higherUsers, $numberOfReturnedUsers): array
    {
        $numberOfSemiUsers = floor($numberOfReturnedUsers / 2);
        $countOfLowerUsers = count($lowerUsers);
        $countOfHigherUsers = count($higherUsers);
        if ($countOfLowerUsers >= $numberOfSemiUsers && $countOfHigherUsers >= $numberOfSemiUsers) {
            $arrayLower = array_slice($lowerUsers, 0, $numberOfSemiUsers);
            $arrayHigher = array_slice($higherUsers, 0, $numberOfSemiUsers);
            return array_merge($arrayHigher, [$mainUser], $arrayLower);
        }
        if ($countOfLowerUsers < $numberOfSemiUsers) {
            $numberOfTakeFromHigher = $numberOfReturnedUsers - 1 - $countOfLowerUsers;
            if ($countOfHigherUsers < $numberOfTakeFromHigher) {
                $numberOfTakeFromHigher = $countOfHigherUsers;
            }
            $arrayHigher = array_slice($higherUsers, 0, $numberOfTakeFromHigher);
            $arrayLower = array_slice($higherUsers, 0, $countOfLowerUsers);
            return array_merge($arrayHigher, [$mainUser], $arrayLower);
        }

        if ($countOfHigherUsers < $numberOfSemiUsers) {
            $numberOfTakeFromLower = $numberOfReturnedUsers - 1 - $countOfHigherUsers;
            if ($countOfLowerUsers < $numberOfTakeFromLower) {
                $numberOfTakeFromLower = $countOfLowerUsers;
            }
            $arrayLower = array_slice($lowerUsers, 0, $numberOfTakeFromLower);
            $arrayHigher = array_slice($higherUsers, 0, $countOfHigherUsers);
            return array_merge($arrayHigher, [$mainUser], $arrayLower);
        }
        return [$countOfHigherUsers];

    }
}
