<?php

namespace App\Http\Controllers\Front;

use App\Enum\CacheType;
use App\Http\Controllers\Controller;
use App\Services\CacheService;
use App\Services\Front\KarmaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;


class KarmaController extends Controller
{

    protected $karmaService;
    protected $cacheService;

    function __construct(KarmaService $karmaService, CacheService $cacheService)
    {
        $this->karmaService = $karmaService;
        $this->cacheService = $cacheService;
    }

    public function getKarmaActions($userId, $limit, $action): JsonResponse
    {
        try {
            $result = $this->cacheService->getItemFromCache(
                CacheType::USER,
                'api/v1/user/',
                ['user_id' => $userId, 'limit' => $limit, 'action' => $action]
            );
            if ($result) {
                return response()->json($result['data'], $result['code']);
            }
            switch ($action) {
                case  'karma-position' :
                    $result = $this->karmaService->getKarma($userId, $limit);
                    $code = $result['code'];
                    $data = $result['data'];
                    break;
                default:
                    $code = 404;
                    $data['message'] = "invalid action";;
            }
            $this->cacheService->saveItemInCache(
                CacheType::USER,
                'api/v1/user/',
                ['user_id' => $userId, 'limit' => $limit, 'action' => $action],
                ['data' => $data, 'code' => $code]
            );
            return response()->json($data, $code);
        } catch (\Exception $e) {
            Log::debug('getKarmaActions Exception :' . $e->getMessage());
            Log::debug($e->getTraceAsString());
            return response()->json(['message' => 'something went wrong'], 500);
        }

    }

}
