<?php

namespace App\Helpers;

use Illuminate\Http\Request;

class FilterHelper
{
    public static function instance(): FilterHelper
    {
        return new FilterHelper();
    }

    public function takeCareOfSearch($request, $normalSearchColumns,$allowedAdvanceSearchColumns, $query){

        $query = $this->takeCareOfNormalSearch($request,$normalSearchColumns,$query);
        $query = $this->takeCareOfAdvancedSearchFilter($request,$allowedAdvanceSearchColumns,$query);
        return $query;
    }

    public function takeCareOfNormalSearch($request, $normalSearchColumns, $query)
    {
        if (isset($request['searchQuery'])) {
            $query->where(function ($query) use ($normalSearchColumns, $request) {
                if(count($normalSearchColumns) > 0){
                    $query->where($normalSearchColumns[0], 'like', '%' . $request['searchQuery'] . '%');
                    foreach ($normalSearchColumns as $item) {
                        $query->orWhere($item, 'like', '%' . $request['searchQuery'] . '%');
                    }
                }
            });
        }
        return $query;
    }

    public function takeCareOfAdvancedSearchFilter($request,$allowedAdvanceSearchColumns, $query)
    {
        if (isset($request['advanceSearchFilter']) && !is_null($request['advanceSearchFilter'])) {
            foreach ($request['advanceSearchFilter'] as $filter) {
                if (isset($filter['value']) && !is_null($filter['value']) && $filter['value'] != '' && !empty($filter['value'])) {
                    if(in_array($filter['key'],$allowedAdvanceSearchColumns)){
                        $query = $this->getFilter($filter, $query);
                    }
                }
            }
        }
        return $query;
    }

    public function getFilter($filter, $query)
    {
        switch ($filter['type']) {
            case 'date' :
                return $this->getDateFilter($filter, $query);
            case 'number' :
                return $this->getNumberFilter($filter, $query);
            case 'text' :
                return $this->getTextFilter($filter, $query);
            case 'multiple-select' :
                return $this->getSelectFilter($filter, $query);
            default :
                return $this->getNormalFilter($filter, $query);
        }
    }

    public function getDateFilter($filter, $query)
    {
        return $query->whereDate($filter['key'], $this->getStrategy($filter['strategy']), $filter['value']);
    }

    public function getStrategy($strategy): string
    {
        switch ($strategy) {
            case 'gt' :
                return '>=';
            case 'lt' :
                return '<=';
            case 'eq' :
                return '=';
        }
        return '=';
    }

    public function getNumberFilter($filter, $query)
    {
        return $query->where($filter['key'], $this->getStrategy($filter['strategy']), $filter['value']);
    }

    public function getTextFilter($filter, $query)
    {
        return $query->where($filter['key'], 'like', '%' . $filter['value'] . '%');
    }

    public function getSelectFilter($filter, $query)
    {
        return $query->whereIn($filter['key'], $filter['value']);
    }

    public function getNormalFilter($filter, $query)
    {
        return $query->where($filter['key'], $filter['value']);
    }


}
