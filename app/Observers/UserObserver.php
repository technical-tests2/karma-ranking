<?php

namespace App\Observers;

use App\Enum\CacheType;
use App\Models\User;
use App\Services\CacheService;

class UserObserver
{

    protected $cacheService;

    function __construct(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * Handle the User "created" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function created(User $user)
    {
        //
        $this->cacheService->removeTagCache(CacheType::USER);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function updated(User $user)
    {
        //
        $this->cacheService->removeTagCache(CacheType::USER);
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
        $this->cacheService->removeTagCache(CacheType::USER);
    }

    /**
     * Handle the User "restored" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function restored(User $user)
    {
        //
        $this->cacheService->removeTagCache(CacheType::USER);
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
        $this->cacheService->removeTagCache(CacheType::USER);
    }
}
