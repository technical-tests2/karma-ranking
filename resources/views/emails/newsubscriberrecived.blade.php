<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<!--Header -->
<div class="header">
    <div class="title">NEW SUBSCRIBER RECEIVED.</div>
</div>
<div class="content">
    <div class="content__item">
        <div class="value">{{$subscriber}} has successfully subscribed to Bohec newsletter</div>
    </div>

    <div class="thank-you">
        <div>Thank you.</div>
        <div>Bohec</div>
    </div>
    <div class="copy-right">
        © 2022 Bohec. All rights reserved.
    </div>

</div>
</body>
</html>
<style>
    @font-face {
        font-family: "Roboto-Regular";
        src: url("{{ url('/fonts/Roboto-Regular.ttf') }}");
    }
    @font-face {
        font-family: "Raleway-Light";
        src: url("{{ url('/fonts/Raleway-Light.ttf') }}");
    }
    @font-face {
        font-family: "Roboto-Light";
        src: url("{{ url('/fonts/Roboto-Light.ttf') }}");
    }
    body{
        padding: 60px;
        font-family: "Roboto-Light", sans-serif;
    }
    .header{
        display: flex;
        justify-content: space-between;
        border-bottom: 3px solid gray;
        padding-bottom: 10px;
    }
    .title{
        font-family:"Raleway-Light", sans-serif;
        font-size: 20px;
        font-weight: bold;
    }
    .content{
        padding-top: 30px;
    }
    .content__item{
        display: flex;
        justify-content: start;
        align-items: start;
        padding-bottom:30px;
    }
    .label{
        width: 100px;
        font-family:"Raleway-Light", sans-serif;
        font-size: 16px;
        font-weight: bold;
    }
    .value{
        flex: 90%;
        font-size: 14px;
    }
    .thank-you{
        margin: 40px 0;
        font-family:"Raleway-Light", sans-serif;
        font-size: 16px;
        font-weight: bold;
    }
    .copy-right{
        font-size: 14px;
    }
</style>
